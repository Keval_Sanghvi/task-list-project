// Define UI elements variables

const form = document.querySelector("#task-form");
const taskList = document.querySelector("#collection-list");
const clearBtn = document.querySelector(".clear-tasks");
const filter = document.querySelector("#filter");
const taskInput = document.querySelector("#task");
//var instance = M.Modal.getInstance(elems);

document.addEventListener('DOMContentLoaded', function () {
    var elems = document.querySelectorAll('.modal');
    M.Modal.init(elems, {});
});

//document.querySelectorAll('.prevent').forEach(item => {
//    item.addEventListener('click', event => {
//        event.preventDefault();
//    });
//});

//document.querySelector(".prevent").addEventListener("click", function (e) {
//    e.preventDefault();
//});

loadEvents();

function loadEvents() {

    // Window Load Event
    document.addEventListener('DOMContentLoaded', getTasks);

    // Form Submit Event
    form.addEventListener('submit', addTask);

    // Remove Task Event
    taskList.addEventListener('click', removeTask);

    // Update Task Event
    taskList.addEventListener('click', updateTask);

    // Clear All Tasks Event
    clearBtn.addEventListener('click', clearTasks);

    // Filter Task Event
    filter.addEventListener('keyup', filterTasks);
}

function createAndAddItemToTaskList(task) {
    // We have to create 'li' and insert to 'ul'

    // create li element
    const li = document.createElement('li');

    // add class names
    li.className = "collection-item";

    // add text in li
    li.appendChild(document.createTextNode(task));

    // create link to delete
    const deleteLink = document.createElement('a');

    // add attribute to link
    deleteLink.setAttribute('href', '#modal1');

    // add class
    deleteLink.className = 'delete-item secondary-content modal-trigger';

    // add 'x' as button in a
    deleteLink.innerHTML = "<i class='fa fa-remove'></i>";

    // append link to li
    li.appendChild(deleteLink);

    // create link to update
    const updateLink = document.createElement('a');

    // add attribute to link
    updateLink.setAttribute('href', '#modal2');

    // add class 
    updateLink.className = 'update-item secondary-content modal-trigger';

    // add 'icon' as button in a
    updateLink.innerHTML = "<i class='fa fa-pencil'></i>";

    // append link to li
    li.appendChild(updateLink);

    // append li to ul
    taskList.appendChild(li);
}

function addTask(e) {
    e.preventDefault();
    if (taskInput.value === '') {
        alert("Please do insert any task!");
    } else {
        createAndAddItemToTaskList(taskInput.value);

        // store to local storage 
        storeTaskInLocalStorage(taskInput.value);

        // clear the task input
        taskInput.value = '';
    }
}

function removeTask(e) {
    e.preventDefault();
    if (e.target.classList.contains('delete-item') || e.target.parentElement.classList.contains('delete-item')) {
        if (document.querySelector(".modal-close").addEventListener("click", function (ee) {
                ee.preventDefault();
                let taskValue;

                if (e.target.parentElement.nodeName === "LI") {
                    taskValue = e.target.parentElement.textContent;
                    e.target.parentElement.remove();
                } else {
                    taskValue = e.target.parentElement.parentElement.textContent;
                    e.target.parentElement.parentElement.remove();
                }
                removeTaskFromLocalStorage(taskValue);
            }));
    }
}

function clearTasks(e) {
    e.preventDefault();
    // slower method
    // taskList.innerHTML = '';

    // faster method
    while (taskList.firstChild) {
        taskList.removeChild(taskList.firstChild);
    }

    // remove all from storage
    let tasks = [];
    localStorage.setItem('tasks', JSON.stringify(tasks));
}

function filterTasks(e) {
    const key = e.target.value.toLowerCase();

    document.querySelectorAll('.collection-item').forEach(function (task) {
        const item = task.firstChild.textContent;
        if (item.toLowerCase().indexOf(key) == -1) {
            task.style.display = 'none';
        } else {
            task.style.display = 'block';
        }
    });
}

function storeTaskInLocalStorage(task) {
    let tasks;
    if (localStorage.getItem('tasks') === null) {
        tasks = [];
    } else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }
    tasks.push(task);
    localStorage.setItem('tasks', JSON.stringify(tasks));
}

function getTasks(e) {
    // retrieve all the tasks from storage and display
    let tasks;
    if (localStorage.getItem('tasks') === null) {
        tasks = [];
    } else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }

    tasks.forEach(function (task) {
        createAndAddItemToTaskList(task);
    });
}

function removeTaskFromLocalStorage(task) {
    let tasks;
    if (localStorage.getItem('tasks') === null) {
        tasks = [];
    } else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }

    tasks.forEach(function (taskValue, index) {
        if (taskValue === task) {
            tasks.splice(index, 1);
        }
    });

    localStorage.setItem('tasks', JSON.stringify(tasks));
}

function updateTask(e) {
    e.preventDefault();
    if (e.target.classList.contains('update-item') || e.target.parentElement.classList.contains('update-item')) {
        if (document.querySelector(".update").addEventListener("click", function (ee) {
                ee.preventDefault();
                let oldTask;
                let upd = document.querySelector("#updating").value;
                document.querySelector("#updating").value = ""
                if (e.target.parentElement.nodeName === 'LI') {
                    oldTask = e.target.parentElement.firstChild.textContent;
                    if (upd === '') {
                        upd = oldTask;
                    }
                    e.target.parentElement.firstChild.textContent = upd;
                } else {
                    oldTask = e.target.parentElement.parentElement.firstChild.textContent;
                    if (upd === '') {
                        upd = oldTask;
                    }
                    e.target.parentElement.parentElement.firstChild.textContent = upd;
                }

                updateTaskFromLocalStorage(oldTask, upd);
            }));
    }
}

function updateTaskFromLocalStorage(oldTask, newTask) {
    let tasks;
    if (localStorage.getItem('tasks') === null) {
        tasks = [];
    } else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }

    tasks.forEach(function (taskValue, index) {
        if (taskValue === oldTask) {
            tasks[index] = newTask;
        }
    });

    localStorage.setItem('tasks', JSON.stringify(tasks));
}
